from django.apps import AppConfig


class MutualappConfig(AppConfig):
    name = 'mutualApp'
